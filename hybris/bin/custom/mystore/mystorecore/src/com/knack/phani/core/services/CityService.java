/**
 *
 */
package com.knack.phani.core.services;

import java.util.List;

import com.knack.phani.core.model.CityModel;


/**
 * @author PhaneeY
 *
 */
public interface CityService
{
	List<CityModel> getAllCityList();

	/**
	 *
	 */
	CityModel getCityByPk(final String pk);
}
