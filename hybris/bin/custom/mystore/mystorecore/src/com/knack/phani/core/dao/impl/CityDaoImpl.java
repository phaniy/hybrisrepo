/**
 *
 */
package com.knack.phani.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.knack.phani.core.dao.CityDao;
import com.knack.phani.core.model.CityModel;


/**
 * @author PhaneeY
 *
 */
public class CityDaoImpl extends AbstractItemDao implements CityDao
{
	@Override
	public List<CityModel> getAllCityList()
	{

		final String queryString = "SELECT {c:" + CityModel.PK + "}" + " FROM {" + CityModel._TYPECODE + " AS c}";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		return getFlexibleSearchService().<CityModel> search(query).getResult();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.knack.phani.core.dao.CityDao#getCityByPk()
	 */
	@Override
	public CityModel getCityByPk(final String pk)
	{
		final String queryString = "SELECT {c:" + CityModel.PK + "}" + " FROM {" + CityModel._TYPECODE + " AS c} where {c:"
				+ CityModel.PK + "}" + "} =?pk }";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter("pk", pk);

		final SearchResult<CityModel> result = getFlexibleSearchService().<CityModel> search(query);
		if (result == null || result.getCount() == 0)
		{
			return null;
		}
		return result.getResult().get(0);
	}

}
