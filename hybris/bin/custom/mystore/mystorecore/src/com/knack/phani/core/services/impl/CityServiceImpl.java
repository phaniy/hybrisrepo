/**
 *
 */
package com.knack.phani.core.services.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.knack.phani.core.dao.CityDao;
import com.knack.phani.core.model.CityModel;
import com.knack.phani.core.services.CityService;


/**
 * @author PhaneeY
 *
 */
public class CityServiceImpl implements CityService
{

	@Autowired
	ModelService modelService;

	private CityDao cityDao;

	public void setCityDao(final CityDao cityDao)
	{
		this.cityDao = cityDao;
	}

	@Override
	public List<CityModel> getAllCityList()
	{
		return cityDao.getAllCityList();
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.knack.phani.core.services.CityService#getCityByPk()
	 */
	@Override
	public CityModel getCityByPk(final String pk)
	{
		return modelService.get(pk);
	}
}
