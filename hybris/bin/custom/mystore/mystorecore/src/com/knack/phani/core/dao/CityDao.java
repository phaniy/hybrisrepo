/**
 *
 */
package com.knack.phani.core.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.knack.phani.core.model.CityModel;


/**
 * @author PhaneeY
 *
 */
public interface CityDao extends Dao
{


	List<CityModel> getAllCityList();


	/**
	 * @param pk
	 * @return
	 */
	CityModel getCityByPk(String pk);

}
