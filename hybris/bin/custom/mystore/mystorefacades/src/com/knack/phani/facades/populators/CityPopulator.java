/**
 *
 */
package com.knack.phani.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.knack.phani.core.model.CityModel;
import com.knack.phani.facades.city.data.CityDto;


/**
 * @author PhaneeY
 *
 */
public class CityPopulator implements Populator<CityModel, CityDto>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final CityModel source, final CityDto target) throws ConversionException
	{
		target.setCityname(source.getCityname());
		target.setCitycountry(source.getCitycountry());
		target.setCoordinates(source.getCoordinates());
		target.setPincode(source.getPincode().toString());
		target.setCityid(source.getPk().toString());
	}

}
