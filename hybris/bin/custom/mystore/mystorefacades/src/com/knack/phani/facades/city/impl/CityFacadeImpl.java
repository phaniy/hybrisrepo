/**
 *
 */
package com.knack.phani.facades.city.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.knack.phani.core.model.CityModel;
import com.knack.phani.core.services.CityService;
import com.knack.phani.facades.city.CityFacade;
import com.knack.phani.facades.city.data.CityDto;


/**
 * @author PhaneeY
 *
 */
public class CityFacadeImpl implements CityFacade
{

	@Autowired
	private ModelService service;
	private CityService cityService;
	private Converter<CityModel, CityDto> cityConverter;

	@Override
	public List<CityDto> getAllCityList()
	{
		final List<CityDto> cityDtoList = new LinkedList<>();

		final List<CityModel> cityModelList = cityService.getAllCityList();
		for (final CityModel cityM : cityModelList)
		{
			cityDtoList.add(cityConverter.convert(cityM));
		}

		return cityDtoList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.knack.phani.facades.city.CityFacade#save(com.knack.phani.core.model.CityModel)
	 */
	@Override
	public void save(final CityModel c) throws ConversionException
	{
		// XXX Auto-generated method stub
		service.save(c);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.knack.phani.facades.city.CityFacade#getCurrentCity()
	 */
	@Override
	public CityDto getCurrentCity(final String pk) throws ConversionException
	{
		return getCityConverter().convert(getCityByPk(pk));
	}


	protected CityModel getCityByPk(final String pk)
	{
		return getCityService().getCityByPk(pk);
	}

	public CityService getCityService()
	{
		return cityService;
	}

	public void setCityService(final CityService cityService)
	{
		this.cityService = cityService;
	}


	public Converter<CityModel, CityDto> getCityConverter()
	{
		return cityConverter;
	}

	public void setCityConverter(final Converter<CityModel, CityDto> cityConverter)
	{
		this.cityConverter = cityConverter;
	}

	/**
	 * @return the service
	 */
	public ModelService getService()
	{
		return service;
	}

	/**
	 * @param service
	 *           the service to set
	 */
	public void setService(final ModelService service)
	{
		this.service = service;
	}


}

