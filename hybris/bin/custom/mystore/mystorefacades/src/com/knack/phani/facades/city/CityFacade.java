/**
 *
 */
package com.knack.phani.facades.city;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import com.knack.phani.core.model.CityModel;
import com.knack.phani.facades.city.data.CityDto;


/**
 * @author PhaneeY
 *
 */
public interface CityFacade
{
	List<CityDto> getAllCityList() throws ConversionException;

	CityDto getCurrentCity(String pk) throws ConversionException;

	void save(CityModel c) throws ConversionException;

}
