<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-header">
	<div class="row">
		<div class="container-lg col-md-6">
			<spring:theme text="City Details" />
		</div>
	</div>
</div>
<div class="row">
	<div class="container-lg col-md-6">
		<div class="account-section-content">
			<div class="account-section-form">
				<form:form action="add-city" method="post" commandName="addcityform">

					<formElement:formSelectBox idKey="profile.cityname"
						labelKey="profile.cityname" path="cityname" inputCSS="text"
						mandatory=" true" />
					<formElement:formInputBox idKey="profile.citycountry"
						labelKey="profile.citycountry" path="citycountry" inputCSS="text"
						mandatory="true" />
					<formElement:formInputBox idKey="profile.pincode"
						labelKey="profile.pincode" path="pincode" inputCSS="text"
						mandatory="true" />
					<formElement:formInputBox idKey="profile.coordinates"
						labelKey="profile.coordinates" path="coordinates" inputCSS="text"
						mandatory="true" />

					<div class="row">
						<div class="col-sm-6 col-sm-push-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_savePersonalDetails_button">
									<button type="submit" class="btn btn-primary btn-block">
										<spring:theme text="Save" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
						<div class="col-sm-6 col-sm-pull-6">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_cancelPersonalDetails_button">
									<button type="button"
										class="btn btn-default btn-block backToHome">
										<spring:theme code="text.account.profile.cancel" text="Cancel" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>