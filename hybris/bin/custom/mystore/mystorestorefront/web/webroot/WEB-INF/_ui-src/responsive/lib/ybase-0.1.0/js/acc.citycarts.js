ACC.savedcarts = {

	_autoload : [
			[ "bindEditConfirmLink", $('.js-edit-city-details').length != 0 ],
			[ "bindAddConfirmLink", $('.js-add-city-details').length != 0 ] ],

	/*
	 * bindEditConfirmLink : function() { $(document).on( "click",
	 * '.js-edit-city-details', function(event) { event.preventDefault(); var
	 * cityId = $(this).data('editcity-id'); var url =
	 * ACC.config.encodedContextPath + '/my-account/city-details/' +
	 * encodeURIComponent(cityId) + '/edit'; ACC.common
	 * .checkAuthenticationStatusBeforeAction(function() { $.ajax({ url : url,
	 * type : 'EDIT', success : function(response) { ACC.colorbox.close(); var
	 * url = ACC.config.encodedContextPath + "/my-account/city-details/" +
	 * encodeURIComponent(cityId) + "/edit" window.location.replace(url); } });
	 * });
	 * 
	 * }); },
	 */
	bindAddConfirmLink : function() {
		$(".js-add-city-details").click(function(event) {
			event.preventDefault();
			var popupTitle = $(this).data('popup-title');
			var url = ACC.config.encodedContextPath + '/my-account/add-city/';
			var popupTitleHtml = ACC.common.encodeHtml(popupTitle);

			$.get(url, undefined, undefined, 'html').done(function(data) {
				ACC.colorbox.open(popupTitleHtml, {
					html : data,
					width : 500,
					onComplete : function() {
						/*
						 * ACC.common.refreshScreenReaderBuffer();
						 * ACC.savedcarts.bindPostAddCityLink();
						 */
					},
					onClosed : function() {
						ACC.common.refreshScreenReaderBuffer();
					}
				});
			});

		});
	},
	bindPostAddCityLink : function() {
		var keepRestoredCart = true;
		var preventSaveActiveCart = false;

		$(document).on("click", '.js-keep-restored-cart', function(event) {
			keepRestoredCart = $(this).prop('checked');
		});

		$(document).on("click", '.js-prevent-save-active-cart',
				function(event) {
					preventSaveActiveCart = $(this).prop('checked');
				});

		$(document)
				.on(
						"click",
						'.js-save-cart-restore-btn',
						function(event) {

							event.preventDefault();
							var cartName = $('#activeCartName').val();
							var url = $(this).data('restore-url');
							var postData = {
								preventSaveActiveCart : preventSaveActiveCart,
								keepRestoredCart : keepRestoredCart,
								cartName : cartName
							};

							ACC.common
									.checkAuthenticationStatusBeforeAction(function() {
										$
												.post(url, postData, undefined,
														'html')
												.done(
														function(result, data,
																status) {
															result = ACC.sanitizer
																	.sanitize(result);
															if (result == "200") {
																var url = ACC.config.encodedContextPath
																		+ "/cart"
																window.location
																		.replace(url);
															} else {
																var errorMsg = status.responseText
																		.slice(
																				1,
																				-1);
																$(
																		'.js-restore-current-cart-form')
																		.addClass(
																				'has-error');
																$(
																		'.js-restore-error-container')
																		.html(
																				errorMsg);
																$('.js-savedcart_restore_confirm_modal').colorbox
																		.resize();
															}
														});
									});
						});

		$(document).on("click", '.js-cancel-restore-btn', function(event) {
			ACC.colorbox.close();
		});
	}
}