<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
table {
	table-layout: fixed;
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

td {
	width: 25%;
}

table td {
	padding: 10px;
	vertical-align: top;
}

td, th {
	border: 1px solid #dddddd;
	text-align: center;
	padding: 10px;
}

tr, td {
	text-align: left;
	padding: 10px;
}

table th:last-child {
	text-align: center;
}

table td, a, button {
	text-align: center;
}

tr:nth-child(even) {
	background-color: #dddddd;
}

td:nth-of-type(1) {
	display: none;
}

th:nth-of-type(1) {
	display: none;
}
</style>
<div style="position: absolute; top: 200px; right: 10px; z-index: 999">
	<%-- <a href="#" class="js-add-city-details restore-item-link"
		data-popup-title="Add City Details"> <span class="hidden-xs"><spring:theme
				text='Add City' /></span> <span class="glyphicon glyphicon-adding"></span>
	</a> --%>
	
	<a href="#" class="js-add-city-details restore-item-link"
									<%-- data-savedcart-id="${fn:escapeXml(savedCart.code)}" --%>
									data-popup-title="<spring:theme code='Add New City'/>">
									<span class="hidden-xs"><spring:theme code='Add New City'/></span>
									<span class="glyphicon glyphicon-share-alt visible-xs"></span>
								</a>
	
</div>
<div class="account-section-header">
	<spring:theme code="text.account.cityDetails" />
</div>

<div class="row">
	<form:form action="city-details" method="get">

		<table>
			<tr>
				<th>Pk</th>
				<th>City</th>
				<th>Country</th>
				<th>Coordinates</th>
				<th>Pincode</th>
			</tr>
			<c:forEach items="${cityDetailsForm}" var="c">
				<tr>
					<td><c:out value="${c.cityid}" /></td>
					<td><c:out value="${c.cityname}" /></td>
					<td><c:out value="${c.citycountry}" /></td>
					<td><c:out value="${c.coordinates}" /></td>
					<td><c:out value="${c.pincode}" /></td>
					<td><a href="#" class="js-edit-city-details"
						data-editcity-id="${fn:escapeXml(c.cityid)}"> <%-- <span
							class="hidden-xs"><spring:theme code='EDIT' /></span> --%> <span
							class="glyphicon glyphicon-edit"></span>
					</a></td>
					<td><a href=""> <span class="glyphicon glyphicon-trash"></span>
					</a></td>
				</tr>
			</c:forEach>
		</table>
		<%-- <div class="container-lg col-md-6">
			<div class="account-section-content">
				<div class="account-section-form">
					<form:form action="city-details" method="post"
						commandName="updateCityDetailsForm">

						<formElement:formSelectBox idKey="profile.title"
							labelKey="profile.title" path="titleCode" mandatory="true"
							skipBlank="false" skipBlankMessageKey="form.select.empty"
							items="${titleData}" selectCSSClass="form-control" />
						<formElement:formInputBox idKey="profile.firstName"
							labelKey="profile.firstName" path="firstName" inputCSS="text"
							mandatory="true" />
						<formElement:formInputBox idKey="profile.lastName"
							labelKey="profile.lastName" path="lastName" inputCSS="text"
							mandatory="true" />
					</form:form>
				</div>
			</div>
		</div> --%>
	</form:form>
</div>