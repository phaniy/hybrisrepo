/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.acceleratorstorefrontcommons.forms;

/**
 * Form object for updating profile.
 */
public class UpdateCityDetailsForm
{

	private String cityName;
	private String cityCountry;
	private String pincode;
	private String coordinates;

	/**
	 * @return the cityName
	 */
	public String getCityName()
	{
		return cityName;
	}

	/**
	 * @param cityName
	 *           the cityName to set
	 */
	public void setCityName(final String cityName)
	{
		this.cityName = cityName;
	}

	/**
	 * @return the cityCountry
	 */
	public String getCityCountry()
	{
		return cityCountry;
	}

	/**
	 * @param cityCountry
	 *           the cityCountry to set
	 */
	public void setCityCountry(final String cityCountry)
	{
		this.cityCountry = cityCountry;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode()
	{
		return pincode;
	}

	/**
	 * @param pincode
	 *           the pincode to set
	 */
	public void setPincode(final String pincode)
	{
		this.pincode = pincode;
	}

	/**
	 * @return the coordinates
	 */
	public String getCoordinates()
	{
		return coordinates;
	}

	/**
	 * @param coordinates
	 *           the coordinates to set
	 */
	public void setCoordinates(final String coordinates)
	{
		this.coordinates = coordinates;
	}




}
