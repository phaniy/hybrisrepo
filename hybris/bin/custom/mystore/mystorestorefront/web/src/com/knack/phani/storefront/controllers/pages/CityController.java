/**
 *
 */
package com.knack.phani.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.knack.phani.core.model.CityModel;
import com.knack.phani.facades.city.CityFacade;
import com.knack.phani.facades.city.data.CityDto;


/**
 * @author PhaneeY
 *
 */
@Controller
@RequestMapping("/my-account")
public class CityController extends AbstractPageController
{

	private static final String CITY_DETAILS_CMS_PAGE = "city-details";
	private static final String ADD_CITY_CMS_PAGE = "add-city";
	private static final String REDIRECT_TO_UPDATE_EMAIL_PAGE = REDIRECT_PREFIX + "/my-account/city-details";
	private static final String ADD_CITY_JSP = "fragments/account/addCity.jsp";

	@Autowired
	private CityFacade cityFacade;


	@RequestMapping(value = "/add-city", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addCityDetails(final Model model) throws CMSItemNotFoundException
	{
		/*
		 * final CityModel cityModel = new CityModel(); model.addAttribute("addcityform", cityModel);
		 * storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_CITY_CMS_PAGE)); setUpMetaDataForContentPage(model,
		 * getContentPageForLabelOrId(ADD_CITY_CMS_PAGE));
		 */
		return ADD_CITY_JSP;
	}

	@RequestMapping(value = "/add-city", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateEmail(final CityModel cityModel, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final String returnAction = REDIRECT_TO_UPDATE_EMAIL_PAGE;

		cityFacade.save(cityModel);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.confirmationUpdated", null);


		return returnAction;

	}


	@RequestMapping(value = "/city-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCityDetails(final Model model) throws CMSItemNotFoundException
	{
		final ArrayList<CityDto> dtos = new ArrayList<>();
		dtos.addAll(cityFacade.getAllCityList());
		model.addAttribute("cityDetailsForm", dtos);
		storeCmsPageInModel(model, getContentPageForLabelOrId(CITY_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CITY_DETAILS_CMS_PAGE));
		return getViewForPage(model);
	}


	/*
	 * @RequestMapping(value = "/city-details/{cityId}/edit", method = RequestMethod.GET)
	 * 
	 * @RequireHardLogIn public String getCityById(@PathVariable(value = "cityId") final String cityId, final Model
	 * model) throws CMSItemNotFoundException { final CityDto citydata = cityFacade.getCurrentCity(cityId); final
	 * UpdateCityDetailsForm updateCityDetailsForm = new UpdateCityDetailsForm();
	 * updateCityDetailsForm.setCityCountry(citydata.getCitycountry());
	 * updateCityDetailsForm.setCityName(citydata.getCityname());
	 * updateCityDetailsForm.setCoordinates(citydata.getCoordinates());
	 * updateCityDetailsForm.setPincode(citydata.getPincode()); model.addAttribute("updateCityDetailsForm",
	 * updateCityDetailsForm);
	 * 
	 * storeCmsPageInModel(model, getContentPageForLabelOrId(CITY_DETAILS_CMS_PAGE)); setUpMetaDataForContentPage(model,
	 * getContentPageForLabelOrId(CITY_DETAILS_CMS_PAGE));
	 * 
	 * return getViewForPage(model); }
	 */


}
